# Run following command, client will connect to localhost port 8888

go run main.go

# Simple protocol

Client files are saved at ./files

You will input file name that you want to send to server

Client check if file is existed or not, if:

    - File is existed, client send command SEND to server:

        SEND filename filesize

    - File is not existed, log that file is not exited

If client receive STOP cmd, it do nothing.

If client receive START cmd, it start transfering file.

