package main

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"os"

	"bitbucket.org/learn-go/serverbitbucket.org/learn-go/client/myclient"
)

func main() {
	// Connect to localhost:8888
	// Do not handle err
	addr, _ := net.ResolveTCPAddr("tcp", "localhost:8888")

	for {
		conn, _ := net.DialTCP("tcp", nil, addr)

		// Read filepath from keyboard input
		reader := bufio.NewReader(os.Stdin)
		fmt.Printf("Input filepath: ")
		buffer, err := reader.ReadBytes('\n')

		// Error handling
		if err != nil {
			log.Println("-> Input error :", err)
		} else {
			filePath := string(buffer[0 : len(buffer)-1])
			myclient.SendCmd(conn, filePath)
		}

		conn.Close()
	}
}
