package myclient

import (
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/learn-go/file-utils/myutils"
)

// SendCmd use when client want to send file
func SendCmd(conn *net.TCPConn, filePath string) {

	if myutils.FileExists("./files/" + filePath) {
		// Prepare message
		// SEND filepath size
		info, _ := os.Stat("./files/" + filePath)
		size := strconv.FormatInt(info.Size(), 10)
		message := "SEND" + " " + filePath + " " + size + "\n"

		// Send message to server
		conn.Write([]byte(message))

		// Commands Receiver
		cmdRecv := receiveCmd(conn)

		handleResponse(conn, cmdRecv, filePath)
	} else {
		fmt.Printf("File %s does not exist (or is a directory)\n", filePath)
	}
}

func receiveCmd(conn *net.TCPConn) (cmdRecv string) {
	// Receive response from server
	buffer2 := make([]byte, 10)
	for {

		c, e := conn.Read(buffer2)
		if e != nil {
			if e == io.EOF {
				fmt.Printf("Server disconnected!!\n")
				break
			} else {
				break
			}
		}

		if c > 0 && buffer2[c-1] == 10 {
			cmdRecv = cmdRecv + string(buffer2[0:c-1])
			break
		} else {
			cmdRecv = cmdRecv + string(buffer2[0:c])
		}
	}
	return
}

func handleResponse(conn *net.TCPConn, cmdRecv string, filePath string) {
	cmd := strings.Split(cmdRecv, " ")
	size2, _ := strconv.ParseInt(cmd[2], 10, 64)

	switch cmd[0] {
	case "START":
		log.Println("Receive command: ", cmdRecv)
		time.Sleep(5000 * time.Millisecond)
		myutils.FileTransfer("./files/"+filePath, conn)
	case "STOP":
		log.Println("Receive command: ", cmdRecv)
		log.Println("File existed on server, size: ", size2)
	}
}
